# Harris Matrix Data Package

Harris Matrix Data Package is a proposal for a standardised digital format of archaeological stratigraphy datasets in CSV format,
following the table schema developed by Thomas S. Dye for the [`hm` Lisp package](http://tsdye.online/harris-matrix/homepage/),
augmented with a metadata descriptor (`datapackage.json`)
that enables consistency checks and streamlined data access with the [Frictionless Data](https://frictionlessdata.io/) tools and programming
libraries.
In the standard, each dataset consists of various CSV tables and a metadata descriptor, forming a _data package_.

This repository contains a Python command-line tool (`hmdp`) that can create, check consistency of datasets with the format and perform various types of analysis on each stratigraphy data package.

## Setting up the environment

Create a Python virtual environment, activate it and run

    pip install .

This will install all dependencies and make the `hmdp` command
available in the virtual environment.

## Development

All source code is formatted with [Black](https://black.readthedocs.io/en/stable/).

A pre-commit file is available in the repository.

## Glossary

In the Frictionless Data glossary:

- _data descriptor_ is a JSON file, named `datapackage.json`, that
  is-found in the top-level directory of a data package, and contains
  metadata about the entire data package (name, description, creation
  date, author names, references) together with the data package
  schema
- _resource_ is a single block of data, such as a CSV table or a
  JSON data file

In the Harris Matrix Data Package:

- each Harris Matrix is a data package
- there is 1 data descriptor
- there are from 2 to 7 CSV tables
- each CSV table is a _resource_

The two resources that MUST be present are:

- _contexts_
- _observations_

Most often, excavation data will make use of three other resources:

- _inferences_
- _periods_
- _phases_

Only in case there are radiocarbon dates or other absolute chronology
available the two resources should be used:

- _events_
- _event-order_

Resource names are standardized so that the _data descriptor_ can remain
largely untouched, except for the specific metadata.

## Using the `hmdp` program from the command line

### Create a new data package

`hmdp init` will assist in the creation of a new data package.

### Check an existing data package


In case something goes wrong, but also if you are experimenting with
the data format, the `check` command is a useful shortcut to run all
possible automated checks.

`hmdp check datapackage.json` will perform three checks on the dataset:

- validate the data descriptor without looking at the data
  (e.g. resources can be missing or broken but the JSON file is well
  formatted), this is equivalent to running `datapackage validate
  datapackage.json`
- validate every resource for internal consistency (e.g. there are
  column headers, each row has the right number of columns,
  constraints like integer values, enums, etc. are respected), this is
  equivalent to running `goodtables datapackage.json` (but in case of
  errors the separate command will give more details)
- check the consistency of foreign keys based on the data descriptor,
  again using the goodtables programming library.

### Create a Harris matrix from the data package

`hmdp matrix datapackage.json` will check stratigraphy data consistency
and output a `matrix.gv` file for processing with Graphviz.

To create a graphical representation of the resulting matrix, the
default procedure is to use the `dot` command, like this:

```
dot matrix.gv -Tpng -o matrix.png
```

## How to cite this work

If you use this software in your research, please provide a citation to
the paper introducing it:

Costa, Stefano. “Una proposta di standard per l’archiviazione e la condivisione di dati stratigrafici.” Archeologia e Calcolatori, 30, 2019, pp. 459–62, DOI: https://doi.org/10.19282/ac.30.2019.29
