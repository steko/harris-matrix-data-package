import click

from datapackage import Package
from goodtables import validate
from tableschema.exceptions import RelationError, CastError


@click.command()
@click.argument("descriptor", type=click.Path(exists=True))
def check(descriptor):
    """Check validity of descriptor, resources and foreign keys."""

    package = Package(descriptor)

    # equivalent of running `datapackage validate`
    print("DATA PACKAGE")
    if package.valid:
        click.echo("datapackage descriptor is valid")
    else:
        raise click.ClickException("datapackage descriptor is NOT valid")

    # equivalent of running `goodtables validate`
    click.echo("RESOURCES")
    report = validate(descriptor, checks=["structure", "schema", "foreign-key"])
    click.echo("there are {} data package resources".format(report["table-count"]))
    click.echo(
        "there are {} errors in the data package resources".format(
            report["error-count"]
        )
    )
    if report["error-count"] > 0:
        for table in report["tables"]:
            if table["error-count"] > 0:
                for error in table["errors"]:
                    click.echo("{}: {}".format(table["source"], error))
        raise click.ClickException("Please correct errors in the data package resources")
