import click

from datapackage import Package
from goodtables import validate
from tableschema.exceptions import RelationError, CastError


@click.command()
@click.argument("descriptor", type=click.Path())
def check(descriptor):
    """Check validity of descriptor, resources and foreign keys."""

    package = Package(descriptor)

    # equivalent of running `datapackage validate`
    print("DATA PACKAGE")
    if package.valid:
        click.echo("datapackage descriptor is valid")
    else:
        click.echo("datapackage descriptor is NOT valid")
        click.echo("---")

    # equivalent of running `goodtables validate`
    click.echo("RESOURCES")
    report = validate(descriptor, checks=["structure", "schema", "foreign-key"])

    click.echo(
        "there are {} errors in the data package resources".format(
            report["error-count"]
        )
    )
    for table in report["tables"]:
        if table["error-count"] > 0:
            for error in table["errors"]:
                click.echo("{}: {}".format(table["source"], error))
