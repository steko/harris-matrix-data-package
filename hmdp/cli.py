import click

from .hm import matrix
from .check_rela import check


@click.group()
def cli():
    """Basic script"""
    click.echo("Harris Matrix Data Package")


cli.add_command(check)
cli.add_command(matrix)
