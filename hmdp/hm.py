import click
import networkx as nx

from datapackage import Package


@click.command()
@click.argument("descriptor", type=click.Path(exists=True))
@click.argument("output", type=click.Path(writable=True), default="matrix.gv")
def matrix(descriptor, output):
    """Read stratigraphy data and output a Graphviz file."""

    package = Package(descriptor)
    contexts = package.get_resource("contexts")
    observations = package.get_resource("observations")
    inferences = package.get_resource("inferences")

    G = nx.DiGraph()

    for context in contexts.read(keyed=True):
        G.add_node(context["label"])
        if context["unit-type"] == "deposit":
            shape = "box"
        elif context["unit-type"] == "interface":
            shape = "trapezium"
        elif context["unit-type"] == "structure":
            shape = "ellipse"
        else:
            shape = "triangle"
        G.nodes[context["label"]]["shape"] = shape

    for observation in observations.read(keyed=True):
        G.add_edge(observation["younger"], observation["older"])

    # inferences about once-equal contexts are less straightforward
    # a common procedure to extract data from context sheets will have many duplicate values
    # therefore contracted_nodes should run after parsing all data on a normalized and
    # deduplicated version
    mapping = {}
    inference_list = []
    for inference in inferences.read():
        normalized = [inference[0], inference[1]]
        normalized.sort()  # this ensures that duplicates will not mess up because of order
        inference_list.append(
            tuple(normalized)
        )  # list is unhashable, which doesn't work with set()
        mapping[normalized[0]] = f"{normalized[0]}={normalized[1]}"
    inference_set = set(inference_list)  # actually removes the duplicates

    for i in inference_set:
        H = nx.contracted_nodes(G, i[0], i[1])
        G = nx.relabel_nodes(H, mapping)

    # P = nx.drawing.nx_agraph.write_dot(G, "prematrix.gv")

    if nx.is_directed_acyclic_graph(G):
        M = nx.algorithms.dag.transitive_reduction(G)

        for attr in ("shape", "color"):
            nx.set_node_attributes(M, nx.get_node_attributes(G, attr), attr)

        M.graph_attr = {"layout": "ortho"}

        P = nx.drawing.nx_agraph.write_dot(M, output)
    else:
        print("FOUND A CYCLE IN THE GRAPH")
        print(list(nx.find_cycle(G)))
