import json
import pathlib
import re

import click

from datapackage import Package

resources_ref = dict()
resources_ref["contexts"] = {
    "path": "contexts.csv",
    "profile": "tabular-data-resource",
    "name": "contexts",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "label", "type": "string", "format": "default"},
            {"name": "unit-type", "type": "string", "format": "default"},
            {"name": "position", "type": "string", "format": "default"},
            {"name": "period", "type": "integer", "format": "default"},
            {"name": "phase", "type": "integer", "format": "default"},
            {"name": "url", "type": "string", "format": "default"},
        ],
        "primaryKey": "label",
        "missingValues": [""],
    },
}
resources_ref["observations"] = {
    "path": "observations.csv",
    "profile": "tabular-data-resource",
    "name": "observations",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "younger", "type": "string", "format": "default"},
            {"name": "older", "type": "string", "format": "default"},
            {"name": "url", "type": "string", "format": "default"},
        ],
        "foreignKeys": [
            {
                "fields": "younger",
                "reference": {"resource": "contexts", "fields": "label"},
            },
            {
                "fields": "older",
                "reference": {"resource": "contexts", "fields": "label"},
            },
        ],
        "missingValues": [""],
    },
}
resources_ref["inferences"] = {
    "path": "inferences.csv",
    "profile": "tabular-data-resource",
    "name": "inferences",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "first", "type": "string", "format": "default"},
            {"name": "second", "type": "string", "format": "default"},
        ],
        "foreignKeys": [
            {
                "fields": "first",
                "reference": {"resource": "contexts", "fields": "label"},
            },
            {
                "fields": "second",
                "reference": {"resource": "contexts", "fields": "label"},
            },
        ],
        "missingValues": [""],
    },
}
resources_ref["periods"] = {
    "path": "periods.csv",
    "profile": "tabular-data-resource",
    "name": "periods",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "id", "type": "integer", "format": "default"},
            {"name": "label", "type": "string", "format": "default"},
            {"name": "attribute", "type": "integer", "format": "default"},
            {"name": "url", "type": "string", "format": "default"},
        ],
        "missingValues": [""],
    },
}
resources_ref["phases"] = {
    "path": "phases.csv",
    "profile": "tabular-data-resource",
    "name": "phases",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "id", "type": "integer", "format": "default"},
            {"name": "label", "type": "string", "format": "default"},
            {"name": "attribute", "type": "integer", "format": "default"},
            {"name": "url", "type": "string", "format": "default"},
        ],
        "missingValues": [""],
    },
}
resources_ref["events"] = {
    "path": "events.csv",
    "profile": "tabular-data-resource",
    "name": "events",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "theta", "type": "integer", "format": "default"},
            {"name": "context", "type": "string", "format": "default"},
            {"name": "lab", "type": "string", "format": "default"},
            {"name": "association", "type": "string", "format": "default"},
        ],
        "foreignKeys": [
            {
                "fields": "context",
                "reference": {"resource": "contexts", "fields": "label"},
            }
        ],
        "missingValues": [""],
    },
}
resources_ref["event-order"] = {
    "path": "event-order.csv",
    "profile": "tabular-data-resource",
    "name": "event-order",
    "format": "csv",
    "mediatype": "text/csv",
    "encoding": "utf-8",
    "schema": {
        "fields": [
            {"name": "older", "type": "string", "format": "default"},
            {"name": "younger", "type": "string", "format": "default"},
        ],
        "foreignKeys": [
            {
                "fields": "older",
                "reference": {"resource": "contexts", "fields": "label"},
            },
            {
                "fields": "younger",
                "reference": {"resource": "contexts", "fields": "label"},
            },
        ],
        "missingValues": [""],
    },
}

def validate_name(ctx, param, value: str) -> bool:
    if re.match(r"^([-a-z0-9._/])+$", value):
        return value
    else:
        raise click.BadParameter("only lowercase Latin letters, digits and ._/")

@click.command()
@click.argument("path", type=click.Path(exists=True, path_type=pathlib.Path))
@click.option(
    "--name",
    prompt="What is the data package short name? (only lowercase Latin letters, digits and ._/)",
    callback=validate_name,
)
@click.option("--author", prompt="Who is the author?")
@click.option(
    "--resource-preset",
    type=click.Choice(
        [
            "contexts-observations",
            "contexts-observations-inferences",
            "contexts-observations-inferences-periods-phases",
            "all",
        ],
        case_sensitive=False,
    ),
    prompt=True,
)
def init(path, name, author, resource_preset):
    """Command to create a new Harris Matrix Data Package from scratch"""
    resources = []
    # list files in the path, if any (TODO)
    # no files: which resource preset?
    if resource_preset:
        resources.append(resources_ref["contexts"])
        with open(path / "contexts.csv", "w") as contexts_file:
            contexts_file.write("label,unit-type,position,period,phase,url")
        resources.append(resources_ref["observations"])
        with open(path / "observations.csv", "w") as observations_file:
            observations_file.write("younger,older,url")
    if resource_preset == "contexts-observations-inferences":
        resources.append(resources_ref["inferences"])
        with open(path / "inferences.csv", "w") as inferences_file:
            inferences_file.write("first,second")
    if resource_preset == "contexts-observations-inferences-periods-phases":
        resources.append(resources_ref["inferences"])
        with open(path / "inferences.csv", "w") as inferences_file:
            inferences_file.write("first,second")
        resources.append(resources_ref["periods"])
        with open(path / "periods.csv", "w") as periods_file:
            periods_file.write("id,label,attribute,url")
        resources.append(resources_ref["phases"])
        with open(path / "phases.csv", "w") as phases_file:
            phases_file.write("id,label,attribute,url")
        resources[0]["foreignKeys"] = [
            {"fields": "period", "reference": {"resource": "periods", "fields": "id"}},
            {"fields": "phase", "reference": {"resource": "phases", "fields": "id"}},
        ]
    if resource_preset == "all":
        resources.append(resources_ref["inferences"])
        with open(path / "inferences.csv", "w") as inferences_file:
            inferences_file.write("first,second")
        resources.append(resources_ref["periods"])
        with open(path / "periods.csv", "w") as periods_file:
            periods_file.write("id,label,attribute,url")
        resources.append(resources_ref["phases"])
        with open(path / "phases.csv", "w") as phases_file:
            phases_file.write("id,label,attribute,url")
        resources[0]["foreignKeys"] = [
            {"fields": "period", "reference": {"resource": "periods", "fields": "id"}},
            {"fields": "phase", "reference": {"resource": "phases", "fields": "id"}},
        ]
        resources.append(resources_ref["events"])
        with open(path / "events.csv", "w") as events_file:
            events_file.write("theta,context,lab,association")
        resources.append(resources_ref["event-order"])
        with open(path / "event-order.csv", "w") as event_order_file:
            event_order_file.write("older,younger")
    # what combinations are acceptable? (TODO)
    # describe inspected resources and ask for confirmation (TODO)
    # ask for metadata
    dp = {
        "name": name,
        "author": author,
        "schema": "tabular-data-package",
        "profile": "tabular-data-package",
        "resources": resources,
    }
    # write descriptor
    with open(path / "datapackage.json", "w") as descriptor:
        json.dump(dp, descriptor)
