from setuptools import find_packages, setup

setup(
    name="hmdp",
    version="0.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
        "networkx",
        "pygraphviz",
        "datapackage",
        "goodtables>=2.4.0",
    ],
    entry_points="""
        [console_scripts]
        hmdp=hmdp.cli:cli
    """,
)
