import importlib.resources
import pathlib
import tempfile
from click.testing import CliRunner
from hmdp.cli import cli


def test_hmdp_check():
    runner = CliRunner()
    result = runner.invoke(cli, ["check", "fig12/datapackage.json"])
    assert result.exit_code == 0


def test_hmdp_check_missing_parameter():
    runner = CliRunner()
    result = runner.invoke(cli, ["check"])
    assert result.exit_code != 0


def test_hmdp_check_ok_output():
    runner = CliRunner()
    result = runner.invoke(cli, ["check", "fig12/datapackage.json"])
    assert (
        result.output
        == """Harris Matrix Data Package
DATA PACKAGE
datapackage descriptor is valid
RESOURCES
there are 7 data package resources
there are 0 errors in the data package resources
"""
    )


# this test should cover the case when descriptor exists but some resources are missing
# (is this a duplicate of the above?)
def test_hmdp_check_missing_data():
    runner = CliRunner()
    result = runner.invoke(cli, ["check", "missing-data/datapackage.json"])
    assert result.exit_code != 0


def test_hmdp_matrix_missing_path():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ["matrix", "non-existing-path/datapackage.json"])
        assert result.exit_code == 2


def test_hmdp_matrix_success():
    runner = CliRunner()
    result = runner.invoke(cli, ["matrix", "fig12/datapackage.json"])
    assert result.exit_code == 0


def test_hmdp_init_preset1():
    runner = CliRunner()
    tempdir = tempfile.mkdtemp()
    result = runner.invoke(
        cli,
        [
            "init",
            "--name",
            "test_preset_1",
            "--author",
            "Test Author 1",
            "--resource-preset",
            "contexts-observations-inferences",
            tempdir,
        ],
    )
    assert result.exit_code == 0
    with pathlib.Path(tempdir) as p:
        for file in [
            "contexts.csv",
            "observations.csv",
            "datapackage.json",
        ]:
            filepath = p / file
            assert filepath.exists() == True


def test_hmdp_init_preset2():
    runner = CliRunner()
    tempdir = tempfile.mkdtemp()
    result = runner.invoke(
        cli,
        [
            "init",
            "--name",
            "test_preset_2",
            "--author",
            "Test Author 2",
            "--resource-preset",
            "contexts-observations-inferences",
            tempdir,
        ],
    )
    assert result.exit_code == 0
    with pathlib.Path(tempdir) as p:
        for file in [
            "contexts.csv",
            "observations.csv",
            "inferences.csv",
            "datapackage.json",
        ]:
            filepath = p / file
            assert filepath.exists() == True


def test_hmdp_init_preset3():
    runner = CliRunner()
    tempdir = tempfile.mkdtemp()
    result = runner.invoke(
        cli,
        [
            "init",
            "--name",
            "test_preset_3",
            "--author",
            "Test Author 3",
            "--resource-preset",
            "contexts-observations-inferences-periods-phases",
            tempdir,
        ],
    )
    assert result.exit_code == 0
    with pathlib.Path(tempdir) as p:
        for file in [
            "contexts.csv",
            "observations.csv",
            "inferences.csv",
            "periods.csv",
            "phases.csv",
            "datapackage.json",
        ]:
            filepath = p / file
            assert filepath.exists() == True


def test_hmdp_init_preset4():
    runner = CliRunner()
    tempdir = tempfile.mkdtemp()
    result = runner.invoke(
        cli,
        [
            "init",
            "--name",
            "test_preset_4",
            "--author",
            "Test Author 4",
            "--resource-preset",
            "all",
            tempdir,
        ],
    )
    assert result.exit_code == 0
    with pathlib.Path(tempdir) as p:
        for file in [
            "contexts.csv",
            "observations.csv",
            "inferences.csv",
            "periods.csv",
            "phases.csv",
            "events.csv",
            "event-order.csv",
            "datapackage.json",
        ]:
            filepath = p / file
            assert filepath.exists() == True
